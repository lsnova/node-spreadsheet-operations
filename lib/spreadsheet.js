var XLSX = require('xlsx');
var escape = require('pg-escape');
var format = require('string-format');

// spreadsheet to array
var sheet2arr = function (sheet) {
    var result = [];
    var row;
    var rowNum;
    var colNum;
    var range = XLSX.utils.decode_range(sheet['!ref']);
    for (rowNum = range.s.r; rowNum <= range.e.r; rowNum++) {
        row = [];
        for (colNum = range.s.c; colNum <= range.e.c; colNum++) {
            var nextCell = sheet[
                XLSX.utils.encode_cell({r: rowNum, c: colNum})
                ];
            if (typeof nextCell === 'undefined') {
                row.push(void 0);
            } else row.push(nextCell.w);
        }
        result.push(row);
    }
    return result;
};


// init format
format.extend(String.prototype);

// extend format with escape and nullable formatters
format.extend(String.prototype, {
    escape    : function (s) {
        return escape('%L', s);
    },
    clean     : function (s) {
        var s2 = s.replace(/-/g, "").replace(/\./g, "");
        return this.escape(s2);
    },
    nullable  : function (s) {
        if (s === null || s === undefined || s === '') {
            return 'null'
        } else {
            return "'" + escape(s) + "'";
        }
    },
    trueornull: function (s) {
        if (true === s) {
            return 'true';
        } else {
            return 'null';
        }
    }
});

module.exports.sheet2arr = sheet2arr;
module.exports.readFile = XLSX.readFile;
